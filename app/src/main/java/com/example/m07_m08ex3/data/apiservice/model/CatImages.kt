package com.example.m07_m08ex3.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class CatImages {
    @SerialName("url") val imageUrl: String =""
}