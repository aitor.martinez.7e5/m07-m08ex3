package com.example.m07_m08ex3.data.apiservice

import com.example.m07_m08ex3.data.apiservice.model.CatImages
import com.example.m07_m08ex3.data.apiservice.model.Cats
import retrofit2.Retrofit
import retrofit2.http.GET
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.http.Path
import retrofit2.http.Query


private const val BASE_URL =
    "https://api.thecatapi.com/"

private val json = Json { this.ignoreUnknownKeys = true }

@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(json.asConverterFactory(("application/json").toMediaType()))
    .baseUrl(BASE_URL)
    .build()

object CatsApi{
    val retrofitService : CatApiService by lazy{
        retrofit.create(CatApiService::class.java)
    }
}
interface CatApiService{
    @GET("/v1/breeds")
    suspend fun getCats(): List<Cats>
    @GET("/v1/images/search")
    suspend fun getCatImageRx(@Query("breed_id")id:String) : List<CatImages>
    @GET("/v1/breeds/{breed_id}")
    suspend fun getCat(@Path(value="breed_id")id:String) : Cats
}