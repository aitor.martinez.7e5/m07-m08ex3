package com.example.m07_m08ex3.ui.model.mapper

import com.example.m07_m08ex3.data.apiservice.model.CatImages
import com.example.m07_m08ex3.data.apiservice.model.Cats
import com.example.m07_m08ex3.ui.model.CatsUIModel

class CatsMapper {
    fun map(breeds: List<Cats>, images: List<CatImages>): List<CatsUIModel> {
        return images.mapIndexed { index, image ->
            CatsUIModel(
                breeds[index].id,
                image.imageUrl,
                breeds[index].name,
                breeds[index].temperament,
                breeds[index].countryCode,
                breeds[index].description,
                breeds[index].wikipedia_url
            )
        }
    }
    fun map2(breeds: Cats, images: CatImages): CatsUIModel {
        return CatsUIModel(
            breeds.id,
            images.imageUrl,
            breeds.name,
            breeds.temperament,
            breeds.countryCode,
            breeds.description,
            breeds.wikipedia_url
        )
    }
}