package com.example.m07_m08ex3.ui.screens


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.m07_m08ex3.data.apiservice.CatsApi
import com.example.m07_m08ex3.ui.model.CatsUIModel
import com.example.m07_m08ex3.ui.model.mapper.CatsMapper

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatsViewModel(id:String): ViewModel() {
    private val _uiState = MutableStateFlow<CatsUIModel>(CatsUIModel("","","","","","",""))
    val uiState: StateFlow<CatsUIModel> = _uiState.asStateFlow()

    private var mapper: CatsMapper = CatsMapper()
    init {
        viewModelScope.launch {
            val breeds = CatsApi.retrofitService.getCat(id)
            val photoListDto = CatsApi.retrofitService.getCatImageRx(id)[0]
            _uiState.value = mapper.map2(breeds, photoListDto)
        }
    }
}